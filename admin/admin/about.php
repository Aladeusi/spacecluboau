<?php
session_start();
//including library
require("php/phpLibrary.php");
$object=new phpLibrary();

$con=$object->startConnection();
?>

<?php

if(isset($_SESSION['adminEmail']))
 {

    //calling initial value out
    $aquery=mysqli_query($con, "SELECT * FROM aboutspace");
    while ($arow=mysqli_fetch_array($aquery)) {
	$ainBrief=$arow['body'];
	$alocation=$arow['location'];
   }
   //calling initial value out


	if($_SERVER['REQUEST_METHOD']=="POST"){

        if(empty($_REQUEST['inBrief'])||empty($_REQUEST['location'])){
        	header("Location:about.php?error=Empty field(s)!    Database not updated.");
        }else{ 
		$body=(trim($_REQUEST['inBrief']));
		$location=(trim($_REQUEST['location']));
		$object->mysqlUpdate2Col("aboutspace","body","location",$body,$location,"body",$ainBrief);
		header("Location:about.php?error=Done! Database updated.");
        }

	}else{





?>

<!DOCTYPE html>
<html>

<!-- Mirrored from cube.adbee.technology/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 20 Dec 2014 10:42:09 GMT -->
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Space club | admin</title>
 
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:1418570549,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok2v=1613a3a185/"},atok:"9fa49c79c599f4a1e6d20e552c6fa421",petok:"1d90be1bee94a344d4dedca7b44a5a0dd53328d9-1419126239-1800",zone:"adbee.technology",rocket:"0",apps:{"ga_key":{"ua":"UA-49262924-2","ga_bs":"2"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="../ajax.cloudflare.com/cdn-cgi/nexp/dok2v%3d919620257c/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
 
<script src="js/demo-rtl.js"></script>
 
 
<link rel="stylesheet" type="text/css" href="css/libs/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="css/libs/nanoscroller.css"/>
 
<link rel="stylesheet" type="text/css" href="css/compiled/theme_styles.css"/>
 
<link rel="stylesheet" href="css/libs/daterangepicker.css" type="text/css"/>
<link rel="stylesheet" href="css/libs/jquery-jvectormap-1.2.2.css" type="text/css"/>
<link rel="stylesheet" href="css/libs/weather-icons.css" type="text/css"/>
 
<link type="image/x-icon" href="img/logo.png" rel="shortcut icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-49262924-2']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script>
</head>
<body>
<div id="theme-wrapper">
<header class="navbar" id="header-navbar">
<div class="container">
<a href="index.php" id="logo" class="navbar-brand">
<img src="img/logo.png" alt="" class="normal-logo logo-white"/>
<img src="img/logo-black.png" alt="" class="normal-logo logo-black"/>
<img src="img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
</a>
<div class="clearfix">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
<ul class="nav navbar-nav pull-left">
<li>
<a class="btn" id="make-small-nav">
<i class="fa fa-bars"></i>
</a>
</li>


<?php 
$inboxCount=count(mysqli_fetch_array(mysqli_query($con,  "SELECT COUNT('id') FROM contactus")));
?>
<li class="dropdown hidden-xs">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-envelope-o"></i>
<span class="count"><?php echo $inboxCount; ?></span>
</a>
<ul class="dropdown-menu notifications-list messages-list">
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>
<?php
//calling inbox out

$iquery=mysqli_query($con, "SELECT * FROM contactus ORDER BY id DESC LIMIT 0,3");
while ($irow=mysqli_fetch_array($iquery)) {
	

?>
<li class="item first-item">
<a href="contact.php">
<img src="img/ghost.png" alt="" style="width:40px; height:40px;border-radius:40px;"/>
<span class="content">
<span class="content-headline">
<?php echo $irow['name']; ?>
</span>
<span class="content-headline">
<?php echo $irow['email']; ?>
</span>
<span class="content-text">
<?php echo $irow['body'];?>
</span>
</span>
<span class="time"><i class="fa fa-clock-o"></i><?php echo $irow['created'];?></span>
</a>
</li>
<?php } ?>

<li class="item-footer">
<a href="contact.php">
View all messages
</a>
</li>
</ul>
</li>


</ul>
</div>
<div class="nav-no-collapse pull-right" id="header-nav">
<ul class="nav navbar-nav pull-right">
<li class="mobile-search">
<a class="btn">
<i class="fa fa-search"></i>
</a>
<div class="drowdown-search">
<form role="search">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search nav-search-icon"></i>
</div>
</form>
</div>
</li>
<li class="dropdown profile-dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<img src="img/samples/scarlet-159.png" alt=""/>
<span class="hidden-xs">Space club admin</span> <b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="contact.php"><i class="fa fa-envelope-o"></i>Messages</a></li>
<li><a href="logOut.php"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</li>
<li class="hidden-xxs">
<a class="btn" href="logOut.php">
<i class="fa fa-power-off"></i>
</a>
</li>
</ul>
</div>
</div>
</div>
</header>
<div id="page-wrapper" class="container">
<div class="row">
<div id="nav-col">
<section id="col-left" class="col-left-nano">
<div id="col-left-inner" class="col-left-nano-content">
<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
<img alt="" src="img/samples/scarlet-159.png"/>
<div class="user-box">
<span class="name">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
Admin
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu">
<li><a href="contact.php"><i class="fa fa-envelope-o"></i>Messages</a></li>
<li><a href="logOut.php"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</span>
<span class="status">
<i class="fa fa-circle"></i> Online
</span>
</div>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
<ul class="nav nav-pills nav-stacked">
<li class="nav-header nav-header-first hidden-sm hidden-xs">
Navigation
</li>
<li class="active">
<a href="index.php">
<i class="fa fa-dashboard"></i>
<span>Dashboard</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>

<li class="active">
<a href="about.php">
<i class="fa fa-th-large"></i>
<span>About club</span>
</a>
</li>


<li class="">
<a href="activity.php">
<i class="fa fa-th-large"></i>
<span>Activities</span>
</a>
</li>



<li class="">
<a href="comments.php">
<i class="fa fa-th-large"></i>
<span>Comments</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>



<li class="">
<a href="contact.php">
<i class="fa fa-th-large"></i>
<span>Contact us</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>


<li>
<a href="#" class="dropdown-toggle">
<i class="fa fa-table"></i>
<span>Gallery</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="images.php">
Images
</a>
</li>
<li>
<a href="videos.php">
Videos
</a>
</li>

</ul>
</li>
<li>
<a href="#" class="dropdown-toggle">
<i class="fa fa-group"></i>
<span>Members</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="excos.php">
Excos
</a>
</li>
<li>
<a href="clubMembers.php">
Club members
</a>
</li>
<li>
<a href="signUpMembers.php">
Sign up members
</a>
</li>
</ul>
</li>


<li class="">
<a href="news.php">
<i class="fa fa-th-large"></i>
<span>News</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>



<li class="">
<a href="subscribers.php">
<i class="fa fa-th-large"></i>
<span>Newsletter subscribers</span>
</a>
</li>




<li class="">
<a href="welcome.php">
<i class="fa fa-th-large"></i>
<span>Welcome address</span>
</a>
</li>



<li class="">
<a href="membership.php">
<i class="fa fa-th-large"></i>
<span>Membership</span>
</a>
</li>








</ul>
</div>
</div>
</section>
<div id="nav-col-submenu"></div>
</div>
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<div id="content-header" class="clearfix">
<div class="pull-left">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li class="active"><span>about</span></li>
</ol>
<h1>About space club O A U</h1>
</div>

</div>
</div>
</div>




<?php if(isset($_GET['error'])){
	?>
<div class="row">
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12" style="background:;">
<div class="main-box feed">
<div class="main-box-body clearfix">
	<br>
<span class="h5"><?php echo $_GET['error'];?></span>
</div>
</div>
</div>
</div>
<?php
}else echo null;
?>



<div class="row">
<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
<div class="main-box feed">
<header class="main-box-header clearfix">
<h2 class="pull-left">Club in brief</h2>
</header>
<div class="main-box-body clearfix">
<?php if($ainBrief==null){
	echo "Null";
}else
{echo $ainBrief;}
?>
</div>
</div>
</div>



<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2>Location</h2>
</header>
<div class="main-box-body clearfix">
<?php if($alocation==null){
	echo "Null";
}else
{echo $alocation;}
?>
</div>
</div>
</div>

</div>














<div class="row">

<div class="col-lg-8 col-md-12 col-xs-12">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>Update about space club here</h2>
</header>
<div class="main-box-body clearfix">
<form role="form" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">


<div class="form-group">
<label for="exampleTextarea">Club in brief</label>
<textarea name="inBrief" class="form-control" id="exampleTextarea" rows="3" id="exampleTooltip" data-toggle="tooltip" data-placement="bottom" title="End each paragraph with '<BR><BR>' "></textarea>
</div>

<div class="form-group">
<label for="exampleTextarea">Location</label>
<textarea name="location" class="form-control" id="exampleTextarea" rows="3" id="exampleTooltip" data-toggle="tooltip" data-placement="bottom" title="End each paragraph with '<BR><BR>' "></textarea>
</div>




<div class="form-group">
<button type="submit" class="btn btn-success">Update</button>
</div>







</form>
</div>
</div>
</div>
</div>




<!--embed-->

<div class="row">

<div class="col-lg-8 col-md-12 col-xs-12">
<div class="main-box">
<header class="main-box-header clearfix">
<h2>See clientside here</h2>
</header>
<div class="main-box-body clearfix">
<iframe src="<?PHP ECHO $object->getSiteDomain() ;?>/about" style="width:100%; height:900px;"></iframe>
</div>
</div>
</div>
</div>



<!--embed-->





</div>
</div>
<footer id="footer-bar" class="row">
<p id="footer-copyright" class="col-xs-12">
Powered by Cloudmacs
</p>
</footer>
</div>
</div>
</div>
</div>
<div id="config-tool" class="closed">
<a id="config-tool-cog">
<i class="fa fa-cog"></i>
</a>
<div id="config-tool-options">
<h4>Layout Options</h4>
<ul>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-header"/>
<label for="config-fixed-header">
Fixed Header
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-sidebar"/>
<label for="config-fixed-sidebar">
Fixed Left Menu
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-footer"/>
<label for="config-fixed-footer">
Fixed Footer
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-boxed-layout"/>
<label for="config-boxed-layout">
Boxed Layout
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-rtl-layout"/>
<label for="config-rtl-layout">
Right-to-Left
</label>
</div>
</li>
</ul>
<br/>
<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
<li>
<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
</a>
</li>
<li>
<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
</a>
</li>
</ul>
</div>
</div>
 
<script src="js/demo-skin-changer.js"></script>  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.nanoscroller.min.js"></script>
<script src="js/demo.js"></script>  
 
<script src="js/moment.min.js"></script>
<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jquery-jvectormap-world-merc-en.js"></script>
<script src="js/gdp-data.js"></script>
<script src="js/flot/jquery.flot.min.js"></script>
<script src="js/flot/jquery.flot.resize.min.js"></script>
<script src="js/flot/jquery.flot.time.min.js"></script>
<script src="js/flot/jquery.flot.threshold.js"></script>
<script src="js/flot/jquery.flot.axislabels.js"></script>
<script src="js/jquery.sparkline.min.js"></script>
<script src="js/skycons.js"></script>
 
<script src="js/scripts.js"></script>
<script src="js/pace.min.js"></script>
 
<script>
	$(document).ready(function() {
		
	    //CHARTS
	    function gd(year, day, month) {
			return new Date(year, month - 1, day).getTime();
		}
		
		if ($('#graph-bar').length) {
			var data1 = [
			    [gd(2015, 1, 1), 838], [gd(2015, 1, 2), 749], [gd(2015, 1, 3), 634], [gd(2015, 1, 4), 1080], [gd(2015, 1, 5), 850], [gd(2015, 1, 6), 465], [gd(2015, 1, 7), 453], [gd(2015, 1, 8), 646], [gd(2015, 1, 9), 738], [gd(2015, 1, 10), 899], [gd(2015, 1, 11), 830], [gd(2015, 1, 12), 789]
			];
			
			var data2 = [
			    [gd(2015, 1, 1), 342], [gd(2015, 1, 2), 721], [gd(2015, 1, 3), 493], [gd(2015, 1, 4), 403], [gd(2015, 1, 5), 657], [gd(2015, 1, 6), 782], [gd(2015, 1, 7), 609], [gd(2015, 1, 8), 543], [gd(2015, 1, 9), 599], [gd(2015, 1, 10), 359], [gd(2015, 1, 11), 783], [gd(2015, 1, 12), 680]
			];
			
			var series = new Array();

			series.push({
				data: data1,
				bars: {
					show : true,
					barWidth: 24 * 60 * 60 * 12000,
					lineWidth: 1,
					fill: 1,
					align: 'center'
				},
				label: 'Revenues'
			});
			series.push({
				data: data2,
				color: '#e84e40',
				lines: {
					show : true,
					lineWidth: 3,
				},
				points: { 
					fillColor: "#e84e40", 
					fillColor: '#ffffff', 
					pointWidth: 1,
					show: true 
				},
				label: 'Orders'
			});

			$.plot("#graph-bar", series, {
				colors: ['#03a9f4', '#f1c40f', '#2ecc71', '#3498db', '#9b59b6', '#95a5a6'],
				grid: {
					tickColor: "#f2f2f2",
					borderWidth: 0,
					hoverable: true,
					clickable: true
				},
				legend: {
					noColumns: 1,
					labelBoxBorderColor: "#000000",
					position: "ne"       
				},
				shadowSize: 0,
				xaxis: {
					mode: "time",
					tickSize: [1, "month"],
					tickLength: 0,
					// axisLabel: "Date",
					axisLabelUseCanvas: true,
					axisLabelFontSizePixels: 12,
					axisLabelFontFamily: 'Open Sans, sans-serif',
					axisLabelPadding: 10
				}
			});

			var previousPoint = null;
			$("#graph-bar").bind("plothover", function (event, pos, item) {
				if (item) {
					if (previousPoint != item.dataIndex) {

						previousPoint = item.dataIndex;

						$("#flot-tooltip").remove();
						var x = item.datapoint[0],
						y = item.datapoint[1];

						showTooltip(item.pageX, item.pageY, item.series.label, y );
					}
				}
				else {
					$("#flot-tooltip").remove();
					previousPoint = [0,0,0];
				}
			});

			function showTooltip(x, y, label, data) {
				$('<div id="flot-tooltip">' + '<b>' + label + ': </b><i>' + data + '</i>' + '</div>').css({
					top: y + 5,
					left: x + 20
				}).appendTo("body").fadeIn(200);
			}
		}
	    
		//WORLD MAP
		$('#world-map').vectorMap({
			map: 'world_merc_en',
			backgroundColor: '#ffffff',
			zoomOnScroll: false,
			regionStyle: {
				initial: {
					fill: '#e1e1e1',
					stroke: 'none',
					"stroke-width": 0,
					"stroke-opacity": 1
				},
				hover: {
					"fill-opacity": 0.8
				},
				selected: {
					fill: '#8dc859'
				},
				selectedHover: {
				}
			},
			markerStyle: {
				initial: {
					fill: '#e84e40',
					stroke: '#e84e40'
				}
			},
			markers: [
				{latLng: [38.35, -121.92], name: 'Los Angeles - 23'},
				{latLng: [39.36, -73.12], name: 'New York - 84'},
				{latLng: [50.49, -0.23], name: 'London - 43'},
				{latLng: [36.29, 138.54], name: 'Tokyo - 33'},
				{latLng: [37.02, 114.13], name: 'Beijing - 91'},
				{latLng: [-32.59, 150.21], name: 'Sydney - 22'},
			],
			series: {
				regions: [{
					values: gdpData,
					scale: ['#6fc4fe', '#2980b9'],
					normalizeFunction: 'polynomial'
				}]
			},
			onRegionLabelShow: function(e, el, code){
				el.html(el.html()+' ('+gdpData[code]+')');
			}
		});

		/* SPARKLINE - graph in header */
		var orderValues = [10,8,5,7,4,4,3,8,0,7,10,6,5,4,3,6,8,9];

		$('.spark-orders').sparkline(orderValues, {
			type: 'bar', 
			barColor: '#ced9e2',
			height: 25,
			barWidth: 6
		});

		var revenuesValues = [8,3,2,6,4,9,1,10,8,2,5,8,6,9,3,4,2,3,7];

		$('.spark-revenues').sparkline(revenuesValues, {
			type: 'bar', 
			barColor: '#ced9e2',
			height: 25,
			barWidth: 6
		});

		/* ANIMATED WEATHER */
		var skycons = new Skycons({"color": "#03a9f4"});
		// on Android, a nasty hack is needed: {"resizeClear": true}

		// you can add a canvas by it's ID...
		skycons.add("current-weather", Skycons.SNOW);

		// start animation!
		skycons.play();

	});
	</script>
</body>

<!-- Mirrored from cube.adbee.technology/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 20 Dec 2014 10:42:54 GMT -->
</html>

<?php
  
$object->closeConnection();

}

}else{
	header("Location:errorPage.php?error=You are not permited to view this page!");
}

?>