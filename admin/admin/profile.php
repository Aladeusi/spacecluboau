
<?php
session_start();
//including library
require("php/phpLibrary.php");
$object=new phpLibrary();

?>

<?php

if(isset($_SESSION['adminEmail']))
{

$con=$object->startConnection();

?>

<!DOCTYPE html>
<html>

<!-- Mirrored from cube.adbee.technology/ by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 20 Dec 2014 10:42:09 GMT -->
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>Space club | admin</title>
 
<script type="text/javascript">
//<![CDATA[
try{if (!window.CloudFlare) {var CloudFlare=[{verbose:0,p:1418570549,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/dok2v=1613a3a185/"},atok:"9fa49c79c599f4a1e6d20e552c6fa421",petok:"1d90be1bee94a344d4dedca7b44a5a0dd53328d9-1419126239-1800",zone:"adbee.technology",rocket:"0",apps:{"ga_key":{"ua":"UA-49262924-2","ga_bs":"2"}}}];!function(a,b){a=document.createElement("script"),b=document.getElementsByTagName("script")[0],a.async=!0,a.src="../ajax.cloudflare.com/cdn-cgi/nexp/dok2v%3d919620257c/cloudflare.min.js",b.parentNode.insertBefore(a,b)}()}}catch(e){};
//]]>
</script>
<link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css"/>
 
<script src="js/demo-rtl.js"></script>
 
 
<link rel="stylesheet" type="text/css" href="css/libs/font-awesome.css"/>
<link rel="stylesheet" type="text/css" href="css/libs/nanoscroller.css"/>
 
<link rel="stylesheet" type="text/css" href="css/compiled/theme_styles.css"/>
 
<link rel="stylesheet" href="css/libs/daterangepicker.css" type="text/css"/>
<link rel="stylesheet" href="css/libs/jquery-jvectormap-1.2.2.css" type="text/css"/>
<link rel="stylesheet" href="css/libs/weather-icons.css" type="text/css"/>
 
<link type="image/x-icon" href="img/logo.png" rel="shortcut icon"/>
 
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
		<script src="js/html5shiv.js"></script>
		<script src="js/respond.min.js"></script>
	<![endif]-->
<script type="text/javascript">
/* <![CDATA[ */
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-49262924-2']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
/* ]]> */
</script>
</head>
<body>
<div id="theme-wrapper">
<header class="navbar" id="header-navbar">
<div class="container">
<a href="index.php" id="logo" class="navbar-brand">
<img src="img/logo.png" alt="" class="normal-logo logo-white"/>
<img src="img/logo-black.png" alt="" class="normal-logo logo-black"/>
<img src="img/logo-small.png" alt="" class="small-logo hidden-xs hidden-sm hidden"/>
</a>
<div class="clearfix">
<button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
<span class="sr-only">Toggle navigation</span>
<span class="fa fa-bars"></span>
</button>
<div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
<ul class="nav navbar-nav pull-left">
<li>
<a class="btn" id="make-small-nav">
<i class="fa fa-bars"></i>
</a>
</li>


<?php 
$inboxCount=count(mysqli_fetch_array(mysqli_query($con,  "SELECT COUNT('id') FROM contactus")));
?>
<li class="dropdown hidden-xs">
<a class="btn dropdown-toggle" data-toggle="dropdown">
<i class="fa fa-envelope-o"></i>
<span class="count"><?php echo $inboxCount; ?></span>
</a>
<ul class="dropdown-menu notifications-list messages-list">
<li class="pointer">
<div class="pointer-inner">
<div class="arrow"></div>
</div>
</li>
<?php
//calling inbox out

$iquery=   mysqli_query($con, "SELECT * FROM contactus ORDER BY id DESC LIMIT 0,3");
while ($irow=mysqli_fetch_array($iquery)) {
	

?>
<li class="item first-item">
<a href="contact.php">
<img src="img/ghost.png" alt="" style="width:40px; height:40px;border-radius:40px;"/>
<span class="content">
<span class="content-headline">
<?php echo $irow['name']; ?>
</span>
<span class="content-headline">
<?php echo $irow['email']; ?>
</span>
<span class="content-text">
<?php echo $irow['body'];?>
</span>
</span>
<span class="time"><i class="fa fa-clock-o"></i><?php echo $irow['created'];?></span>
</a>
</li>
<?php } ?>

<li class="item-footer">
<a href="contact.php">
View all messages
</a>
</li>
</ul>
</li>


</ul>
</div>
<div class="nav-no-collapse pull-right" id="header-nav">
<ul class="nav navbar-nav pull-right">
<li class="mobile-search">
<a class="btn">
<i class="fa fa-search"></i>
</a>
<div class="drowdown-search">
<form role="search">
<div class="form-group">
<input type="text" class="form-control" placeholder="Search...">
<i class="fa fa-search nav-search-icon"></i>
</div>
</form>
</div>
</li>
<li class="dropdown profile-dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<img src="img/samples/scarlet-159.png" alt=""/>
<span class="hidden-xs">Space club admin</span> <b class="caret"></b>
</a>
<ul class="dropdown-menu">
<li><a href="contact.php"><i class="fa fa-envelope-o"></i>Messages</a></li>
<li><a href="logOut.php"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</li>
<li class="hidden-xxs">
<a class="btn" href="logOut.php">
<i class="fa fa-power-off"></i>
</a>
</li>
</ul>
</div>
</div>
</div>
</header>
<div id="page-wrapper" class="container">
<div class="row">
<div id="nav-col">
<section id="col-left" class="col-left-nano">
<div id="col-left-inner" class="col-left-nano-content">
<div id="user-left-box" class="clearfix hidden-sm hidden-xs dropdown profile2-dropdown">
<img alt="" src="img/samples/scarlet-159.png"/>
<div class="user-box">
<span class="name">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
Admin
<i class="fa fa-angle-down"></i>
</a>
<ul class="dropdown-menu">
<li><a href="contact.php"><i class="fa fa-envelope-o"></i>Messages</a></li>
<li><a href="logOut.php"><i class="fa fa-power-off"></i>Logout</a></li>
</ul>
</span>
<span class="status">
<i class="fa fa-circle"></i> Online
</span>
</div>
</div>
<div class="collapse navbar-collapse navbar-ex1-collapse" id="sidebar-nav">
<ul class="nav nav-pills nav-stacked">
<li class="nav-header nav-header-first hidden-sm hidden-xs">
Navigation
</li>
<li class="active">
<a href="index.php">
<i class="fa fa-dashboard"></i>
<span>Dashboard</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>

<li >
<a href="about.php">
<i class="fa fa-th-large"></i>
<span>About club</span>
</a>
</li>


<li class="">
<a href="activity.php">
<i class="fa fa-th-large"></i>
<span>Activities</span>
</a>
</li>



<li class="">
<a href="comments.php">
<i class="fa fa-th-large"></i>
<span>Comments</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>



<li class="">
<a href="contact.php">
<i class="fa fa-th-large"></i>
<span>Contact us</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>


<li>
<a href="#" class="dropdown-toggle">
<i class="fa fa-table"></i>
<span>Gallery</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="images.php">
Images
</a>
</li>
<li>
<a href="videos.php">
Videos
</a>
</li>

</ul>
</li>
<li>
<a href="#" class="dropdown-toggle">
<i class="fa fa-group"></i>
<span>Members</span>
<i class="fa fa-angle-right drop-icon"></i>
</a>
<ul class="submenu">
<li>
<a href="excos.php">
Excos
</a>
</li>
<li>
<a href="clubMembers.php">
Club members
</a>
</li>
<li>
<a href="signUpMembers.php">
Sign up members
</a>
</li>
</ul>
</li>


<li class="">
<a href="news.php">
<i class="fa fa-th-large"></i>
<span>News</span>
<span class="label label-primary label-circle pull-right">28</span>
</a>
</li>




<li class="">
<a href="subscribers.php">
<i class="fa fa-th-large"></i>
<span>Newsletter subscribers</span>
</a>
</li>




<li class="">
<a href="welcome.php">
<i class="fa fa-th-large"></i>
<span>Welcome address</span>
</a>
</li>



<li class="">
<a href="membership.php">
<i class="fa fa-th-large"></i>
<span>Membership</span>
</a>
</li>








</ul>
</div>
</div>
</section>
<div id="nav-col-submenu"></div>
</div>
<div id="content-wrapper">
<div class="row">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<ol class="breadcrumb">
<li><a href="#">Home</a></li>
<li><a href="#">Users</a></li>

<li class="active"><span>User Profile</span></li>
</ol>
<br>
<div class="profile-label">
	
&nbsp;&nbsp;&nbsp;<a class="label label-success" href="<?php echo $_REQUEST['page'];?>">Go back</a>

</div>
<br>

<h1>User Profile</h1>
</div>
</div>

<?php

$id=$_REQUEST['id'];

$query=   mysqli_query($con, "SELECT * FROM memberlog WHERE id='$id'");

while ($row=mysqli_fetch_assoc($query)) {
	

?>


<div class="row" id="user-profile">
<div class="col-lg-3 col-md-4 col-sm-4">
<div class="main-box clearfix">
<header class="main-box-header clearfix">
<h2><?php echo $row['firstname']." ".$row['othernames'];?></h2>
</header>
<div class="main-box-body clearfix">

<img src="<?php echo $row['picPath'];?>" alt="" class="profile-img img-responsive center-block"/>
<div class="profile-label">
<span class="label label-danger">Member</span>
</div>
<br>
<div class="profile-since">
Member since: <?php echo $row['created'];?>
</div>

</div>
</div>
</div>
<div class="col-lg-9 col-md-8 col-sm-8">
<div class="main-box clearfix">
<div class="tabs-wrapper profile-tabs">
<ul class="nav nav-tabs">
<li class="active"><a href="#tab-newsfeed" data-toggle="tab">Profile</a></li>
</ul>

<div class="tab-content">
<div class="tab-pane fade in active" id="tab-newsfeed">
<div id="newsfeed">
<div class="story">
  <div><label class="label label-success">Email</label>&nbsp;&nbsp;<?php echo $row['email'];?></div><br>
  <div><label class="label label-primary">Firstname</label>&nbsp;&nbsp;<?php echo $row['firstname'];?></div><br>
  <div><label class="label label-danger">Othernames</label>&nbsp;&nbsp;<?php echo $row['othernames'];?></div><br>
    <div><label class="label label-success">Matrix no</label>&nbsp;&nbsp;<?php echo $row['matrixNo'];?></div><br>
  <div><label class="label label-primary">Faculty</label>&nbsp;&nbsp;<?php echo $row['faculty'];?></div><br>
  <div><label class="label label-danger">Department</label>&nbsp;&nbsp;<?php echo $row['department'];?></div><br>
    <div><label class="label label-success">Class</label>&nbsp;&nbsp;<?php echo $row['class'];?></div><br>
  <div><label class="label label-primary">Address</label>&nbsp;&nbsp;<?php echo $row['address'];?></div><br>
  <div><label class="label label-danger">Phone</label>&nbsp;&nbsp;<?php echo $row['phone'];?></div><br>
    <div><label class="label label-success">Sex</label>&nbsp;&nbsp;<?php echo $row['sex'];?></div><br>
  <div><label class="label label-primary">DOB</label>&nbsp;&nbsp;<?php echo $row['DOB'];?></div><br>
  <div><label class="label label-danger">Country</label>&nbsp;&nbsp;<?php echo $row['country'];?></div><br>
    <div><label class="label label-success">Next of kin name</label>&nbsp;&nbsp;<?php echo $row['nextOfKin'];?></div><br>
  <div><label class="label label-primary">Next of kin Address</label>&nbsp;&nbsp;<?php echo $row['nextOfKinAdd'];?></div><br>
  <div><label class="label label-danger">Next of kin phone</label>&nbsp;&nbsp;<?php echo $row['nextOfKinPhone'];?></div><br>
    <div><label class="label label-success">Date joined</label>&nbsp;&nbsp;<?php echo $row['dateJoined'];?></div><br>
</div>

</div>
</div>







</div>
</div>
</div>
</div>
</div>

<?php
}
?>
</div>
</div>
<footer id="footer-bar" class="row">
<p id="footer-copyright" class="col-xs-12">
Powered by Cloudmacs.
</p>
</footer>
</div>
</div>
</div>
</div>
<div id="config-tool" class="closed">
<a id="config-tool-cog">
<i class="fa fa-cog"></i>
</a>
<div id="config-tool-options">
<h4>Layout Options</h4>
<ul>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-header"/>
<label for="config-fixed-header">
Fixed Header
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-sidebar"/>
<label for="config-fixed-sidebar">
Fixed Left Menu
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-fixed-footer"/>
<label for="config-fixed-footer">
Fixed Footer
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-boxed-layout"/>
<label for="config-boxed-layout">
Boxed Layout
</label>
</div>
</li>
<li>
<div class="checkbox-nice">
<input type="checkbox" id="config-rtl-layout"/>
<label for="config-rtl-layout">
Right-to-Left
</label>
</div>
</li>
</ul>
<br/>
<h4>Skin Color</h4>
<ul id="skin-colors" class="clearfix">
<li>
<a class="skin-changer" data-skin="" data-toggle="tooltip" title="Default" style="background-color: #34495e;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-white" data-toggle="tooltip" title="White/Green" style="background-color: #2ecc71;">
</a>
</li>
<li>
<a class="skin-changer blue-gradient" data-skin="theme-blue-gradient" data-toggle="tooltip" title="Gradient">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-turquoise" data-toggle="tooltip" title="Green Sea" style="background-color: #1abc9c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-amethyst" data-toggle="tooltip" title="Amethyst" style="background-color: #9b59b6;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-blue" data-toggle="tooltip" title="Blue" style="background-color: #2980b9;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-red" data-toggle="tooltip" title="Red" style="background-color: #e74c3c;">
</a>
</li>
<li>
<a class="skin-changer" data-skin="theme-whbl" data-toggle="tooltip" title="White/Blue" style="background-color: #3498db;">
</a>
</li>
</ul>
</div>
</div>
 
<script src="js/demo-skin-changer.js"></script>  
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.nanoscroller.min.js"></script>
<script src="js/demo.js"></script>  
 
<script src="js/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&amp;sensor=false"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
 
<script src="js/scripts.js"></script>
<script src="js/pace.min.js"></script>
 
<script type="text/javascript">
	    // When the window has finished loading create our google map below
	    google.maps.event.addDomListener(window, 'load', init);
	    
	    function init() {
	    	var latlng = new google.maps.LatLng(40.763986, -73.958674);
	    	
	        //APPLE MAP
	        var mapOptionsApple = {
	            zoom: 12,
	            scrollwheel: false,
	            center: latlng,
	
	            // How you would like to style the map. 
	            // This is where you would paste any style found on Snazzy Maps.
	            styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]}]
	        };
	
	        var mapElementApple = document.getElementById('map-apple');
	
	        // Create the Google Map using out element and options defined above
	        var mapApple = new google.maps.Map(mapElementApple, mapOptionsApple);
	        
	        var markerApple = new google.maps.Marker({
	    		position: latlng,
	    		map: mapApple
	    	});
	    }
	    
		$(document).ready(function() {
			$('.conversation-inner').slimScroll({
		        height: '340px'
		    });
		});
		
		$(function() {
			$(document).ready(function() {
				$('#newsfeed .story-images').magnificPopup({
					type: 'image',
					delegate: 'a',
					gallery: {
						enabled: true
				    }
				});
			});
		});
	
	</script>
</body>

<!-- Mirrored from cube.adbee.technology/user-profile.html by HTTrack Website Copier/3.x [XR&CO'2013], Sat, 20 Dec 2014 10:44:00 GMT -->
</html>




<?php

$object->closeConnection();
}else{
	header("Location:errorPage.php?error=You are not permited to view this page!");
}

?>