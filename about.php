
<?php
session_start();

//including library
require('php/phpLibrary.php');
//object
$object= new phpLibrary(); 
$con=$object->startConnection();
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <title>spaceclub | About</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">@import url('css/club.css');</style>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" type="text/css" href="plugin/fontAwesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="plugin/malihu/css/jquery.mCustomScrollbar.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="plugin/malihu/js/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="js/club.js"></script>
     
    <!--custom script here-->
    <script type="text/javascript">
    //malihu script
    $(document).ready(function ($) { 


// custom scrollbar api
         $(".scrollDiv").mCustomScrollbar({
          setHeight:295,
          setWidth:false,
          scrollbarPosition: "inside",
          theme:"dark",
          scrollInertia:0
        }); 



       });


    //jssor script

    


    //clubCustom script

    function subscribe(){

                var xmlhttp;

      if(window.XMLHttpRequest){

       xmlhttp = new XMLHttpRequest();           //creating an object for the users with browsers that support xmlhttp


      }else{

       xmlhttp = new ActiveXobject("Microsoft.XMLHTTP");

      }

      var userurl = document.getElementById('semail').value;

       xmlhttp.onreadystatechange = function(){

       if (xmlhttp.readyState==4){
       var processResponse=xmlhttp.responseText;
             document.getElementById('showresults').innerHTML = '<div class="alert_msg" style="color:white;padding:10px;background:#CC0033; font-size:70%;">'+processResponse+'</div><br>';
               






       }

  }
       url ="submitSubscribe.php?email="+userurl;    //taking the form through the name given to it in the form
         xmlhttp.open("GET",url, true);                                    //the'true' in this line of code makes it possible to search
           xmlhttp.send();

    }

    </script>
    <!--custom script here-->


  </head>



  <body style="background:url('img/wrapper.jpg'); background-size:100% 100%;">
     
     <header>
   

        <?php 
    require('navBar.php');
    ?>




<br><br><br>

<div class="row fontSergueL" style="position:relative; top:-22px; z-index:-1;" >
<div class="col-lg-12 cWrapper" style="background-image: url('img/wrapper.png'); background-size:100% 100%;">
    <center class="fColorWhite">
      <br>
      <img src="img/logo.png" class="imgSize-md">
      <div class="h1" style="font-weight:bolder;">Space club in brief.</div>
      <h1 class="h2">O A U, Nigeria.</h1>
      <br>
      

    </center>

  </div>

</div>

 </header>


<div class="mainBody row">

  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10" 
  style="background-color:#303030 ; border-radius:30px 30px 0px 0px; height:60%; padding:5px; position:relative; top:-50px; z-index:1;">
   
    <div class="">

<!--row 1-->
     <div class="row resizeE" style="margin:0px 0px 0px 0px;">

          <div class="col-lg-4 fColorWhite">
            <div class="row">
            <div class="col-lg-offset-8 hidden-sm hidden-md hidden-xs ">
              <br><br><br>
              <ul class="aboutTrigger">
             <li class="aboutClubTrigger"><i class="fa fa-home ">&nbsp;&nbsp;</i>The club</li>
             <li class="aboutExcosTrigger"><i class="fa fa-group ">&nbsp;&nbsp;</i>Excos</li>
            </ul>
            </div>

            <div class=" hidden-lg ">
              <br><br><br>
              <ul class="aboutTrigger">
             <li class="aboutClubTrigger" style="display:inline; padding:10px; border-radius:10px 10px 0 0;"><i class="fa fa-home ">&nbsp;&nbsp;</i>The club</li>
             <li class="aboutExcosTrigger" style="display:inline; padding:10px; border-radius:10px 10px 0 0;"><i class="fa fa-group ">&nbsp;&nbsp;</i>Excos</li>
            </ul>
            </div>
            </div>
            
          </div>

          <div class="col-lg-offset- col-lg-8  fontSergueL" style="padding:15px; background:#505050 ; border-radius:0px 25px 0px 0px;">



          <!--club-->  
                 
          
            <!--in brief---->  
          <div class="club">
            <div class="row">
              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:black;padding:25px; margin:0px 15px 15px 15px;">

                             <div class="row">
                               <div class="peaker col-lg-8 col-sm-8 col-md-8 col-xs-12  fColorWhite bgWelcome pad-md fontSizeM">
                               Club In brief
                               </div>
                             </div>


                                  <div class="row" style="background:white; border-radius:5px;">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8">
                                     <img src="img/about.jpg" style="height:250px; width:100%; height:180px;">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hidden-xs">
                                      <img src="img/rocket.jpg" style="height:250px; width:100%; height:180px;">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 hidden-xs">
                                      <img src="img/wrapper.jpg" style="height:250px; width:100%; height:180px;">
                                    </div>
                                  </div>
                                  <br><br>
   <div paragraph>                               
<p>

<?php
$iquery=mysqli_query( $con,"SELECT * FROM aboutspace");
while($irow=mysqli_fetch_array($iquery)){
   $location=$irow['location'];
   echo $irow['body'];
}

?>

</p> 
</div>

              </div>
              </div>

               <!--in brief-->  



                <!--in brief---->  

            <div class="row">
              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:black;padding:25px; margin:0px 15px 15px 15px;">

                             <div class="row">
                               <div class="peaker col-lg-8 col-sm-8 col-md-8 col-xs-12  fColorWhite bgWelcome pad-md fontSizeM">
                                Activities
                               </div>
                             </div>


                                   <div class="row" style="background:white;border-radius:5px;">
                                   
                                  <?php
                                    $count=1;
                                  $nquery=mysqli_query( $con,"SELECT * FROM activity ORDER BY id  LIMIT 0,3");
                                   while($nrow=mysqli_fetch_assoc($nquery)){
    
                                   $path=explode(",", $nrow['picPath']);
                                      if($count>1){$hidden="col-xs-4 hidden-xs";}else{$hidden="col-xs-8";}

                                  ?>

                                   <div class="col-lg-4 col-md-4 col-sm-4 <?php echo $hidden;?>">
                                     <img src="admin/admin/<?php echo $path[0];?>" style="height:180px; width:100%;">
                                    </div>
                                
                                    <?php
                                      if($count==3){
                                         break;
                                      }
                                     $count++; }
                                    ?>
                                  </div>
                                  <br><br>
   <div paragraph>
    <?php
     $aquery=mysqli_query($con,"SELECT * FROM activity ORDER BY id DESC");
    while($arow=mysqli_fetch_array($aquery)){
     
   


    ?>

   <label class="webLabel-sm"><?php echo strtoupper($arow['header']);?></label>&nbsp;&nbsp; 
   <hr>                         
<p><?php echo $arow['body'];?></p>
<br><br> 
<?php 
}
?>



</div>

              </div>
              </div>

               <!--in brief-->  



 <!--location---->  

            <div class="row">
              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:black;padding:25px; margin:0px 15px 15px 15px;">

                             <div class="row">

                               <div class="peaker col-lg-8 col-sm-8 col-md-8 col-xs-12  fColorWhite bgWelcome pad-md fontSizeM">
                                Location
                                <label class="webLabel-sm floatRight">Map</label> 
                               </div>
                             </div>


                                   <div class="row" style="background:white;">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                                     <iframe width="100%" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com.ng/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Obafemi+Awolowo+University,+Ife,+Osun&amp;aq=0&amp;oq=oba&amp;sll=9.084576,8.674253&amp;sspn=12.149569,18.918457&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=7.517722,4.526348&amp;spn=0.006295,0.006295&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://www.google.com.ng/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Obafemi+Awolowo+University,+Ife,+Osun&amp;aq=0&amp;oq=oba&amp;sll=9.084576,8.674253&amp;sspn=12.149569,18.918457&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=&amp;ll=7.517722,4.526348&amp;spn=0.006295,0.006295&amp;iwloc=A" style="color:#0000FF;text-align:left"></a></small>
                                    </div>
                                    
                                  </div>
                                  <br><br>
   <div googleMap>  

  <p>
  <label class="webLabel-sm floatLeft">Lacation</label><br><hr>
  <?php echo $location;?>
  </p> 
  </div> 

</div>

              </div>
              <!--Location-->  
            </div>





        <div class="excos">
              

                     <?php
                     $equery=mysqli_query($con, "SELECT * FROM excoslog");
                     while ($erow=mysqli_fetch_array($equery)) {
                       

                     ?>

                     <div class="row">
              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:black;padding:25px; margin:0px 15px 15px 15px;">
                     <!--in brief-->


                             <div class="row">
                               <div class="peaker col-lg-8 col-sm-8 col-md-8 col-xs-12  fColorWhite bgWelcome pad-md fontSizeM">
                                <?php echo $erow['post'];?>
                               </div>
                             </div>


                                  <div class="row" style="background:black; border-radius:10px;">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"  style="border:solid white 1px;">
                                     <img src="admin/admin/<?php echo $erow['picPath'];?>" style="height:150px; width:120px; border-radius:10px;">
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 fColorWhite pad-lg-t-b">

                                      <?php echo $erow['firstname'];?>&nbsp;<?php echo $erow['othernames'];?>
                                    </div>
                                    <div class="col-lg-4">
                                      
                                    </div>
                                  </div>
                                  <br><br>
   <div paragraph>
   <LABEL CLASS="webLabel-sm">About <?php echo $erow['firstname'];?></LABEL> 
   <hr>
   <p><?php echo $erow['bio'];?></p> 
    <br><br><br>

    <LABEL CLASS="webLabel-sm">Connect with <?php echo $erow['firstname'];?></LABEL>
   <hr>
   <p><i class="fa fa-phone-square"></i> : <?php echo $erow['phone'];?></p><br> 
   <p><i class="fa fa-twitter-square"></i> : <?php echo $erow['twitter'];?></p><br>
   <p><i class="fa fa-globe"></i> : <?php echo $erow['website'];?></p><br>  

</div>

              <br><br>


              </div>
               


              </div>


               <!--in brief-->  
<?php
}
?>






            
              






              

          </div>

         

          
       

     </div>
     <!--row 1-->



     <!--row 2-->

<br><br>

</div>
  </div>


























<!--footer-->

<?php

$object->addSection('footer.php');


?>

    <!--footer-->


 
    
















    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>




</html>




<?php


$object->closeConnection($con);

 ?>