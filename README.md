![space.jpg](https://bitbucket.org/repo/kjqbgR/images/4090142908-space.jpg)

# SPACE CLUB. Affiliated to ARCSTE-E #

Space Club , the brain child of the Centre for Space Science and Technology Education, (CSSTE) was conceived as one of the catch-them-young-initiatives geared towards a sustained effort in sensitizing, educating, informing and creating awareness for students of tertiary institutions at all levels in all aspects of space technology and its benefits of mankind.

It was launched by Chief of Air Staff, Air Marshall Paul Dike, on the 18th of May, 2007 at the Conference Centre, Obafemi Awolowo University Campus, Ile-Ife, Osun State, Nigeria.

[About space club](https://www.spacecluboau.pe.hu/about)* 

[Contact space club](https://www.spacecluboau.pe.hu/contact)* 

[Become a registered member](https://www.spacecluboau.pe.hu/membership)* 

[Subscribe for newsleter](https://www.spacecluboau.pe.hu/signup)

Mail us at info@spacecluboau.pe.hu