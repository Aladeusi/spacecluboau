<?php
session_start();

//including library
require('php/phpLibrary.php');
//object
$object= new phpLibrary(); 
$con=$object->startConnection();
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <title>spaceclub | Sign</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
        
    
    <style type="text/css">@import url('css/club.css');</style>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" type="text/css" href="plugin/fontAwesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="plugin/malihu/css/jquery.mCustomScrollbar.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="plugin/malihu/js/jquery.mCustomScrollbar.js"></script>
    <!-- Insert to your webpage before the </head> -->
    <script src="sliderengine/amazingslider.js"></script>
    <link rel="stylesheet" type="text/css" href="sliderengine/amazingslider-1.css">
    <script src="sliderengine/initslider-1.js"></script>
    <!-- End of head section HTML codes -->
    <script type="text/javascript" src="js/club.js"></script>

      
    
    <!--custom script here-->
    <script type="text/javascript">
    //malihu script
    $(document).ready(function ($) { 


// custom scrollbar api
         $(".scrollDiv").mCustomScrollbar({
          setHeight:295,
          setWidth:false,
          scrollbarPosition: "inside",
          theme:"dark",
          scrollInertia:0
        }); 



       });


    //jssor script

    


    //clubCustom script

    function subscribe(){

                var xmlhttp;

      if(window.XMLHttpRequest){

       xmlhttp = new XMLHttpRequest();           //creating an object for the users with browsers that support xmlhttp


      }else{

       xmlhttp = new ActiveXobject("Microsoft.XMLHTTP");

      }

      var userurl = document.getElementById('semail').value;

       xmlhttp.onreadystatechange = function(){

       if (xmlhttp.readyState==4){
       var processResponse=xmlhttp.responseText;
             document.getElementById('showresults').innerHTML = '<div class="alert_msg" style="color:white;padding:10px;background:#CC0033; font-size:70%;">'+processResponse+'</div><br>';
               






       }

  }
       url ="submitSubscribe.php?email="+userurl;    //taking the form through the name given to it in the form
         xmlhttp.open("GET",url, true);                                    //the'true' in this line of code makes it possible to search
           xmlhttp.send();

    }

    </script>
    <!--custom script here-->


  </head>



  <body class=".cBodyStyle">
     
     <header>
   
<?php 
    require('navBar.php');
    ?>




<br><br><br>

<div class="row fontSergueL" style="position:relative; top:-22px; z-index:-1;" >
<div class="col-lg-12 cWrapper" style="background-image: url('img/wrapper.png'); background-size:100% 100%;">
    <center class="fColorWhite">
      <br>
      <img src="img/logo.png" class="imgSize-md">
      <div class="h1" style="font-weight:bolder;">Space club playbook, photobook store</div>
      <h1 class="h2">O A U, Nigeria.</h1>
      <br>
    

    </center>

  </div>

</div>

 </header>


<div class="mainBody row">

  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10" 
  style="background-color:#303030 ; border-radius:30px 30px 0px 0px; height:60%; padding:5px; position:relative; top:-50px; z-index:1;">
   
    <div class="">

<!--row 1-->
     <div class="row resizeE" style="margin:0px 0px 0px 0px;">




      <!--controller for mobile-->

      <?PHP if(!isset($_GET['filter']) || $_GET['filter']=="video"){
   $des1="background:#CC3300;";
   $des2=null;
}else{
   $des2="background:#CC3300;";
   $des1=null;
}?>
<div class="col-lg-4 hidden-lg fColorWhite ">
            <div class="row">
            <div class="col-lg-offset-2 col-lg-8 hidden- hidden- hidden- ">
              <br><br><br>
              <ul class="galleryUl">
             <li class="" style="<?php echo $des1;?>"><a href="gallery?filter=video">Videos</a></li>
             <li style="<?php echo $des2;?>"><a href="gallery?filter=photo">Photos</a></li>

            </ul>
            </div>

            
            </div>
            <br><br>
          </div>


<!--controller for mobile-->

          

          <div class="col-lg-offset- col-lg-8  fontSergueL" style="padding:15px; background:#505050 ; border-radius:25px 0px 0px 0px;">



          <!--club-->  
                 
          
            
          <div class="club">

            <!--gallery row---->  
            <div class="row">

              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:black;padding:25px; margin:0px 15px 15px 15px;">
        



     <?php //calling video and photo out    ?>      


      <?php
     if(!isset($_GET['filter']) || $_GET['filter']=="video"){

                   
                   $count=1;
                   $vquery=mysqli_query($con,"SELECT * FROM activity ORDER BY id DESC");
                   while ($vrow=mysqli_fetch_array($vquery)) {
                        
                         $vid=explode(",", $vrow['vidPath']);
                            
                          foreach ($vid as $value) {
                               if($value=="null" || $value==""){continue;}
                                   if($count==10){
                                   echo '
                                      <script type="text/javascript">
                                            $(document).ready(function ($) {

                                              $(".vidTag").hide();
                                                 $("#moreBut").click(function(){
                                                    $(".vidTag").slideDown(function(){
                                                        $("#moreBut").fadeOut();
                                                    });
                                                 }); 

                                            });
                                      </script>  ';
                                      }


                                      if($count>=10){
                                        $showId="vidTag";
                                      }else{$showId=null;}

                                ?>
                            
                                   


                    <!--video1-->
                            <div class="row <?php echo $showId; ?>" style="background:;">
                            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                            <iframe width="100%" height="350px" src="<?php echo $value; ?>" frameborder="0" allowfullscreen></iframe>

                            </div>
                            <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                            <br>
                            <a href="<?php echo $value; ?>" class="webBut-sm">Download</a>
                            <br><br>
                            <label><?php echo $vrow['header']; ?></label>
                            <hr>
                            </div>
                                   
                            </div>

                            <br> 
                    <!--video1-->



                               <?php     
                           
                          $count++;}


                  $count++; }







     }elseif ($_GET['filter']=="photo") {
         echo '<div class="row" style="background:;">';
         $count=1;
                   $vquery=mysqli_query($con, "SELECT * FROM activity ORDER BY id DESC");
                   while ($vrow=mysqli_fetch_array($vquery)) {
                        
                         $pid=explode(",", $vrow['picPath']);
                            
                          foreach ($pid as $value) {
                               if($value=="gallery/img/" || $value==""){continue;}
                                   if($count==10){
                                   echo '
                                      <script type="text/javascript">
                                            $(document).ready(function ($) {

                                              $(".vidTag").hide();
                                                 $("#moreBut").click(function(){
                                                    $(".vidTag").slideDown(function(){
                                                        $("#moreBut").fadeOut();
                                                    });
                                                 }); 

                                            });
                                      </script>  ';
                                      }


                                      if($count>=10){
                                        $showId="vidTag";
                                      }else{$showId=null;}

                                ?>
                            
                                   


                    <!--video1-->
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo $showId; ?>">
                            <img width="100%" height="300px" class="imgGal" src="admin/admin/<?php echo $value; ?>" />

                            </div>

          
                                   
                          
                           
                             
                    <!--video1-->



                               <?php     
                           
                          $count++;}


                  $count++; }






                  $vquery=mysqli_query($con, "SELECT * FROM news ORDER BY id DESC");
                   while ($vrow=mysqli_fetch_array($vquery)) {
                        
                         $pid=explode(",", $vrow['picPath']);
                            
                          foreach ($pid as $value) {
                               if($value=="gallery/img/" || $value==""){continue;}
                                  


                                      if($count>0){
                                        $showId="vidTag";
                                      }else{$showId=null;}

                                ?>
                            
                                   


                    <!--card1-->
                            
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 <?php echo $showId; ?>">
                            <img width="100%" height="300px" class="imgGal" src="admin/admin/<?php echo $value; ?>" />

                            </div>
                            
          
                                   
                          
                           
                             
                    <!--card1-->



                               <?php     
                           
                          }


                  }

                  echo "</div>";
     }

      ?>                   

<?php //calling video and photo out?>

                                  
   

              </div>
              </div>

               <!--in brief-->  





  <div class="row">
   
    &nbsp;&nbsp;&nbsp;
      <button class="webBut" id="moreBut">More</button>
 <br><br>
  </div>
              <!--Location-->  
            </div>








              </div>



<!--controller-->

<div class="col-lg-4 fColorWhite ">
            <div class="row">
            <div class="col-lg-offset-2 col-lg-8 hidden- hidden- hidden- ">
              <br><br><br>
              <ul class="galleryUl">
             <li class="" style="<?php echo $des1;?>"><a href="gallery?filter=video">Videos</a></li>
             <li style="<?php echo $des2;?>"><a href="gallery?filter=photo">Photos</a></li>

            </ul>
            </div>

            
            </div>
            
          </div>

<!--controller-->



              






              

          </div>

         

          
       

     </div>
     <!--row 1-->



     <!--row 2-->

<br><br>

</div>
  </div>



</div>









<!--footer-->

<?php

$object->addSection('footer.php');


?>

    <!--footer-->









    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>




</html>




<?php


$object->closeConnection($con);

 ?>