
<?php
//including library
require('php/phpLibrary.php');
//object
$object = new phpLibrary(); 
 $con=$object->startConnection();
?>



<?php

if($_SERVER['REQUEST_METHOD']=="POST"){
  
  $fileName=$_FILES['upload']['name'];
  $fileNameArray=explode(".", $fileName);
  $format=end($fileNameArray);
  $fileTmp=$_FILES['upload']['tmp_name'];
  $fileSize=$_FILES['upload']['size'];
  $fileType=$_FILES['upload']['type'];
  $jemail=trim($_REQUEST['email']);
  $jpass=trim($_REQUEST['password']);
  $jcpass=trim($_REQUEST['confirmPassword']);
  $jfirstName=trim($_REQUEST['firstName']);
  $jotherNames=trim($_REQUEST['othertNames']);
  $jphone=trim($_REQUEST['phone']);
  $jsex=trim($_REQUEST['sex']);
  $jdob=trim($_REQUEST['dob']);
  $jcountry=trim($_REQUEST['country']);


  if(empty($jemail)||empty($jpass)||empty($jcpass)||empty($jfirstName)||empty($jotherNames)||empty($jphone)||empty($jsex)||empty($jdob)||empty($jcountry)){
    header("Location:signUp.php?error=A field was left blank");
  }else{

   $q = "SELECT COUNT(*) AS Count FROM memberlog WHERE memberType='signUpMember' AND email='$jemail'";
    $result = mysqli_query($con,$q);
    $result = mysqli_fetch_assoc($result);
    $count = $result['Count'];
  

  if($count>=1){
         header("Location:signUp.php?error=$jemail has been used");
  }else{
  
  

    if($jpass==$jcpass){

      $dir="./users/profile/".$jemail;
      mkdir($dir, 0777, true);
       $dirP="users/profile/".$jemail."/".$jemail.".".$format;
     
   if($format=="jpg" || $format=="png" || $format=="bmp" || $format=="jpeg"){
       if($fileSize<=250000){


 
    
     
      
      move_uploaded_file($fileTmp, $dirP);
    }else{
      header("Location:signUp.php?error=Picture too large. Use 250KB or less.");
     }
    }else{
      header("Location:signUp.php?error=Unsupported picture format.");
    }
    


    
  $object->mysqlInsert10Col("memberlog","email","password","firstname","othernames","phone","sex","DOB","country","picPath","memberType",$jemail,$jpass,$jfirstName,$jotherNames,$jphone,$jsex,$jdob,$jcountry,$dirP,"signUpMember");


  header("Location:signIn.php?error=Congratulations! You've successfully signed up.<br>Sign in bellow.");
}else{
   header("Location:signUp.php?error=Passwords did not match.");

   }
}

}

}
else{




?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <title>spaceclub | SinUp</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">@import url('css/club.css');</style>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" type="text/css" href="plugin/fontAwesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="plugin/malihu/css/jquery.mCustomScrollbar.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="plugin/malihu/js/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="js/club.js"></script>
     
    <!--custom script here-->
    <script type="text/javascript">
    //malihu script
    $(document).ready(function ($) { 


// custom scrollbar api
         $(".scrollDiv").mCustomScrollbar({
          setHeight:295,
          setWidth:false,
          scrollbarPosition: "inside",
          theme:"dark",
          scrollInertia:0
        }); 



       });


    //jssor script

    


  
    //clubCustom script

    function subscribe(){

                var xmlhttp;

      if(window.XMLHttpRequest){

       xmlhttp = new XMLHttpRequest();           //creating an object for the users with browsers that support xmlhttp


      }else{

       xmlhttp = new ActiveXobject("Microsoft.XMLHTTP");

      }

      var userurl = document.getElementById('semail').value;

       xmlhttp.onreadystatechange = function(){

       if (xmlhttp.readyState==4){
       var processResponse=xmlhttp.responseText;
             document.getElementById('showresults').innerHTML = '<div class="alert_msg" style="color:white;padding:10px;background:#CC0033; font-size:70%;">'+processResponse+'</div><br>';
               






       }

  }
       url ="submitSubscribe.php?email="+userurl;    //taking the form through the name given to it in the form
         xmlhttp.open("GET",url, true);                                    //the'true' in this line of code makes it possible to search
           xmlhttp.send();

    }

    </script>
    <!--custom script here-->
  </head>


  <body class=".cBodyStyle">
     
     <header>
   
   <?php 
    require('navBar.php');
    ?>



<br><br><br>

<div class="row fontSergueL" style="position:relative; top:-22px; z-index:-1;" >
<div class="col-lg-12 cWrapper" style="background-image: url('img/wrapper.png'); background-size:100% 100%;">
    <center class="fColorWhite">
      <br>
      <img src="img/logo.png" class="imgSize-md">
      <div class="h1" style="font-weight:bolder;">Join us in our quest to explore the space.</div>
      <h1 class="h2">O A U, Nigeria.</h1>
      <br>
      

    </center>

  </div>

</div>

 </header>


<div class="mainBody row">

  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10" 
  style="background-color:#303030 ; border-radius:30px 30px 0px 0px; height:60%; padding:5px; position:relative; top:-50px; z-index:1;">
   



  <!--import box-->
  <br><br><br>
         <div class="row fontSergueL">
     
     
          <div style="border-left:white solid 0px; " class="col-sm-3 col-md-4 col-lg-3">
             <div style="border:white; background-color:inherit;" class="thumbnail">
       </div>
       </div>
     
                   <div style="border-left:white solid 0px; " class="col-sm-6 col-md-4 col-lg-6">
                     <div style="border:white; background-color:inherit;" class="thumbnail">
     
     <label class="webLabel-lg">Join space club here</label>
     <br><br>

<div style="background-color:white; padding:12px;"><br>
<p style="font-size:20px;float:left; font-family:;">Sign up here</p><br><hr>

<form role="form" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
<div class="form-group">
     <?php 
     if(isset($_REQUEST['error'])){
      echo "<label class='webLabel-lg'>".$_REQUEST['error']."</label><br>";
     }

     ?>
    <label >Email</label>
    <input type="email" name="email" class="form-control" id="" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input required type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
  </div>

    <div class="form-group">
    <label required for="exampleInputPassword1">Confirm password</label>
    <input type="password" name="confirmPassword" class="form-control" id="" placeholder="">
  </div>

   <div class="form-group">
    <label>Firstname</label>
    <input required type="text" name="firstName" class="form-control" id="" placeholder="">
  </div>

   <div class="form-group">
    <label>Othernames</label>
    <input required type="text" name="othertNames" class="form-control" id="" placeholder="">
  </div>

   <div class="form-group">
    <label>Phone</label>
    <input required type="text" name="phone" class="form-control" id="" placeholder="">
  </div>

   <div class="form-group">
    <label>Sex</label>
    <select class="form-control" name="sex">
  <option value="male">Male</option>
  <option value="female" selected="selected">Female</option>

</select>
  </div>

   <div class="form-group">
    <label>Date of birth</label>
    <input required type="text" name="dob" class="form-control" id="" placeholder="dd/mm/yyy">
  </div>


     <div class="form-group">
    <label>Country</label>
    <select class="form-control" name="country" id="countries_select" class="m-wrap span8 select2" required><option value="CC" disabled="disabled" selected="selected">Country</option><option value="AD">ANDORRA</option><option value="AE">UNITED ARAB EMIRATES</option><option value="AF">AFGHANISTAN</option><option value="AG">ANTIGUA AND BARBUDA</option><option value="AI">ANGUILLA</option><option value="AL">ALBANIA</option><option value="AM">ARMENIA</option><option value="AN">NETHERLANDS ANTILLES</option><option value="AO">ANGOLA</option><option value="AQ">ANTARCTICA</option><option value="AR">ARGENTINA</option><option value="AS">AMERICAN SAMOA</option><option value="AT">AUSTRIA</option><option value="AU">AUSTRALIA</option><option value="AW">ARUBA</option><option value="AZ">AZERBAIJAN</option><option value="BA">BOSNIA AND HERZEGOVINA</option><option value="BB">BARBADOS</option><option value="BD">BANGLADESH</option><option value="BE">BELGIUM</option><option value="BF">BURKINA FASO</option><option value="BG">BULGARIA</option><option value="BH">BAHRAIN</option><option value="BI">BURUNDI</option><option value="BJ">BENIN</option><option value="BL">SAINT BARTHELEMY</option><option value="BM">BERMUDA</option><option value="BN">BRUNEI DARUSSALAM</option><option value="BO">BOLIVIA</option><option value="BR">BRAZIL</option><option value="BS">BAHAMAS</option><option value="BT">BHUTAN</option><option value="BW">BOTSWANA</option><option value="BY">BELARUS</option><option value="BZ">BELIZE</option><option value="CA">CANADA</option><option value="CC">COCOS (KEELING) ISLANDS</option><option value="CD">CONGO, THE DEMOCRATIC REPUBLIC OF THE</option><option value="CF">CENTRAL AFRICAN REPUBLIC</option><option value="CG">CONGO</option><option value="CH">SWITZERLAND</option><option value="CI">COTE D IVOIRE</option><option value="CK">COOK ISLANDS</option><option value="CL">CHILE</option><option value="CM">CAMEROON</option><option value="CN">CHINA</option><option value="CO">COLOMBIA</option><option value="CR">COSTA RICA</option><option value="CU">CUBA</option><option value="CV">CAPE VERDE</option><option value="CX">CHRISTMAS ISLAND</option><option value="CY">CYPRUS</option><option value="CZ">CZECH REPUBLIC</option><option value="DE">GERMANY</option><option value="DJ">DJIBOUTI</option><option value="DK">DENMARK</option><option value="DM">DOMINICA</option><option value="DO">DOMINICAN REPUBLIC</option><option value="DZ">ALGERIA</option><option value="EC">ECUADOR</option><option value="EE">ESTONIA</option><option value="EG">EGYPT</option><option value="ER">ERITREA</option><option value="ES">SPAIN</option><option value="ET">ETHIOPIA</option><option value="FI">FINLAND</option><option value="FJ">FIJI</option><option value="FK">FALKLAND ISLANDS (MALVINAS)</option><option value="FM">MICRONESIA, FEDERATED STATES OF</option><option value="FO">FAROE ISLANDS</option><option value="FR">FRANCE</option><option value="GA">GABON</option><option value="GB">UNITED KINGDOM</option><option value="GD">GRENADA</option><option value="GE">GEORGIA</option><option value="GH">GHANA</option><option value="GI">GIBRALTAR</option><option value="GL">GREENLAND</option><option value="GM">GAMBIA</option><option value="GN">GUINEA</option><option value="GQ">EQUATORIAL GUINEA</option><option value="GR">GREECE</option><option value="GT">GUATEMALA</option><option value="GU">GUAM</option><option value="GW">GUINEA-BISSAU</option><option value="GY">GUYANA</option><option value="HK">HONG KONG</option><option value="HN">HONDURAS</option><option value="HR">CROATIA</option><option value="HT">HAITI</option><option value="HU">HUNGARY</option><option value="ID">INDONESIA</option><option value="IE">IRELAND</option><option value="IL">ISRAEL</option><option value="IM">ISLE OF MAN</option><option value="IN">INDIA</option><option value="IQ">IRAQ</option><option value="IR">IRAN, ISLAMIC REPUBLIC OF</option><option value="IS">ICELAND</option><option value="IT">ITALY</option><option value="JM">JAMAICA</option><option value="JO">JORDAN</option><option value="JP">JAPAN</option><option value="KE">KENYA</option><option value="KG">KYRGYZSTAN</option><option value="KH">CAMBODIA</option><option value="KI">KIRIBATI</option><option value="KM">COMOROS</option><option value="KN">SAINT KITTS AND NEVIS</option><option value="KP">KOREA DEMOCRATIC PEOPLES REPUBLIC OF</option><option value="KR">KOREA REPUBLIC OF</option><option value="KW">KUWAIT</option><option value="KY">CAYMAN ISLANDS</option><option value="KZ">KAZAKSTAN</option><option value="LA">LAO PEOPLES DEMOCRATIC REPUBLIC</option><option value="LB">LEBANON</option><option value="LC">SAINT LUCIA</option><option value="LI">LIECHTENSTEIN</option><option value="LK">SRI LANKA</option><option value="LR">LIBERIA</option><option value="LS">LESOTHO</option><option value="LT">LITHUANIA</option><option value="LU">LUXEMBOURG</option><option value="LV">LATVIA</option><option value="LY">LIBYAN ARAB JAMAHIRIYA</option><option value="MA">MOROCCO</option><option value="MC">MONACO</option><option value="MD">MOLDOVA, REPUBLIC OF</option><option value="ME">MONTENEGRO</option><option value="MF">SAINT MARTIN</option><option value="MG">MADAGASCAR</option><option value="MH">MARSHALL ISLANDS</option><option value="MK">MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF</option><option value="ML">MALI</option><option value="MM">MYANMAR</option><option value="MN">MONGOLIA</option><option value="MO">MACAU</option><option value="MP">NORTHERN MARIANA ISLANDS</option><option value="MR">MAURITANIA</option><option value="MS">MONTSERRAT</option><option value="MT">MALTA</option><option value="MU">MAURITIUS</option><option value="MV">MALDIVES</option><option value="MW">MALAWI</option><option value="MX">MEXICO</option><option value="MY">MALAYSIA</option><option value="MZ">MOZAMBIQUE</option><option value="NA">NAMIBIA</option><option value="NC">NEW CALEDONIA</option><option value="NE">NIGER</option><option value="Nigeria">NIGERIA</option><option value="NI">NICARAGUA</option><option value="NL">NETHERLANDS</option><option value="NO">NORWAY</option><option value="NP">NEPAL</option><option value="NR">NAURU</option><option value="NU">NIUE</option><option value="NZ">NEW ZEALAND</option><option value="OM">OMAN</option><option value="PA">PANAMA</option><option value="PE">PERU</option><option value="PF">FRENCH POLYNESIA</option><option value="PG">PAPUA NEW GUINEA</option><option value="PH">PHILIPPINES</option><option value="PK">PAKISTAN</option><option value="PL">POLAND</option><option value="PM">SAINT PIERRE AND MIQUELON</option><option value="PN">PITCAIRN</option><option value="PR">PUERTO RICO</option><option value="PT">PORTUGAL</option><option value="PW">PALAU</option><option value="PY">PARAGUAY</option><option value="QA">QATAR</option><option value="RO">ROMANIA</option><option value="RS">SERBIA</option><option value="RU">RUSSIAN FEDERATION</option><option value="RW">RWANDA</option><option value="SA">SAUDI ARABIA</option><option value="SB">SOLOMON ISLANDS</option><option value="SC">SEYCHELLES</option><option value="SD">SUDAN</option><option value="SE">SWEDEN</option><option value="SG">SINGAPORE</option><option value="SH">SAINT HELENA</option><option value="SI">SLOVENIA</option><option value="SK">SLOVAKIA</option><option value="SL">SIERRA LEONE</option><option value="SM">SAN MARINO</option><option value="SN">SENEGAL</option><option value="SO">SOMALIA</option><option value="SR">SURINAME</option><option value="ST">SAO TOME AND PRINCIPE</option><option value="SV">EL SALVADOR</option><option value="SY">SYRIAN ARAB REPUBLIC</option><option value="SZ">SWAZILAND</option><option value="TC">TURKS AND CAICOS ISLANDS</option><option value="TD">CHAD</option><option value="TG">TOGO</option><option value="TH">THAILAND</option><option value="TJ">TAJIKISTAN</option><option value="TK">TOKELAU</option><option value="TL">TIMOR-LESTE</option><option value="TM">TURKMENISTAN</option><option value="TN">TUNISIA</option><option value="TO">TONGA</option><option value="TR">TURKEY</option><option value="TT">TRINIDAD AND TOBAGO</option><option value="TV">TUVALU</option><option value="TW">TAIWAN, PROVINCE OF CHINA</option><option value="TZ">TANZANIA, UNITED REPUBLIC OF</option><option value="UA">UKRAINE</option><option value="UG">UGANDA</option><option value="US">UNITED STATES</option><option value="UY">URUGUAY</option><option value="UZ">UZBEKISTAN</option><option value="VA">HOLY SEE (VATICAN CITY STATE)</option><option value="VC">SAINT VINCENT AND THE GRENADINES</option><option value="VE">VENEZUELA</option><option value="VG">VIRGIN ISLANDS, BRITISH</option><option value="VI">VIRGIN ISLANDS, U.S.</option><option value="VN">VIET NAM</option><option value="VU">VANUATU</option><option value="WF">WALLIS AND FUTUNA</option><option value="WS">SAMOA</option><option value="XK">KOSOVO</option><option value="YE">YEMEN</option><option value="YT">MAYOTTE</option><option value="ZA">SOUTH AFRICA</option><option value="ZM">ZAMBIA</option><option value="ZW">ZIMBABWE</option></select>
  </div>


  <div class="form-group">
    <label for="exampleInputFile">Upload profile picture&nbsp; <br><br><i style="color:red;">* 250kb maximum</i></label>
    <input  type="file" id="" name="upload">
      </div>
<center>
  <button type="submit" class="webBut">Sign up</button>
</center>
  
</form>
  
 </div>
 

 
 
                          </div>
                  </div>
               
                                        <div style="border-left:white solid 0px; " class="col-sm-6 col-md-4">
                                                    <div style="border:white; background-color:inherit;" class="thumbnail">
                                               </div>
                                               </div> 
        
                                
 
             </div>
       </div>
  <!--import box-->




</div>
  </div>



</div>



















<!--footer-->

<?php

$object->addSection('footer.php');


?>

    <!--footer-->





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>




</html>


<?php


$object->closeConnection($con);
}
 ?>