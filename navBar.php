



      

    <nav class="navbar navbar-inverse fontSergueB navbar-fixed-top" role="navigation">
  <div class="col-lg-offset-1 col-lg-10 ">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="index">Home </a></li>
        <li><a href="about">About <span class="sr-only">(current)</span></a></li>
        <li><a href="contact">Contact</a></li>
        <li><a href="membership">Membership</a></li>
        <li><a href="gallery">Gallery</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Activities <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
           

            <?php
          $cquery=mysqli_query($con, "SELECT header FROM activity ORDER BY id DESC");
          while ($crow=mysqli_fetch_array($cquery)) {

            ?>
            <li><a href="activity?getContent=<?php echo $crow['header'];?>"><?php echo $crow['header'];?></a></li>

            <?php
             }
            ?>
            
           
          </ul>
        </li>
      </ul>
   
      <ul class="nav navbar-nav navbar-right">
        <?php  if(isset($_SESSION['email'])){
          ?>
          <li><a href="#"><img src="<?php echo $_SESSION['picPath'];?>" style="width:20px; height:20px; border-radius:25px; border:solid white 1px;"  ></a></li>
         <li><a href="#"><?php echo $_SESSION['firstname'];?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|</a></li>
       <?php }
        ?>

        <li class=""><a href="news">News</a></li>
<?php  if(isset($_SESSION['email'])){
        echo null;
           }else{
        ?>
         <li><a href="signUp">Sign up</a></li>
          <li><a href="signIn">Log in</a></li>
<?php }?>
          <?php  if(isset($_SESSION['email'])){
          ?>
         <li><a href="logOut.php">Log out</a></li>
       <?php }
        ?>
      
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
