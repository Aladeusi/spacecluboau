<?php
session_start();

//including library
require('php/phpLibrary.php');
//object
$object= new phpLibrary(); 
$con=$object->startConnection();



?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <title>spaceclub | Sign</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/logo.png" />
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">@import url('css/club.css');</style>
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/main1.css">
    <link rel="stylesheet" type="text/css" href="plugin/fontAwesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="plugin/malihu/css/jquery.mCustomScrollbar.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="plugin/malihu/js/jquery.mCustomScrollbar.js"></script>
    <script type="text/javascript" src="js/club.js"></script>
     
    <!--custom script here-->
    <script type="text/javascript">
    //malihu script
    $(document).ready(function ($) { 


// custom scrollbar api
         $(".scrollDiv").mCustomScrollbar({
          setHeight:295,
          setWidth:false,
          scrollbarPosition: "inside",
          theme:"dark",
          scrollInertia:0
        }); 



       });


    //jssor script

    


    //clubCustom script

    function subscribe(){

                var xmlhttp;

      if(window.XMLHttpRequest){

       xmlhttp = new XMLHttpRequest();           //creating an object for the users with browsers that support xmlhttp


      }else{

       xmlhttp = new ActiveXobject("Microsoft.XMLHTTP");

      }

      var userurl = document.getElementById('semail').value;

       xmlhttp.onreadystatechange = function(){

       if (xmlhttp.readyState==4){
       var processResponse=xmlhttp.responseText;
             document.getElementById('showresults').innerHTML = '<div class="alert_msg" style="color:white;padding:10px;background:#CC0033; font-size:70%;">'+processResponse+'</div><br>';
               






       }

  }
       url ="submitSubscribe.php?email="+userurl;    //taking the form through the name given to it in the form
         xmlhttp.open("GET",url, true);                                    //the'true' in this line of code makes it possible to search
           xmlhttp.send();

    }

    </script>
    <!--custom script here-->


  </head>



  <body class=".cBodyStyle">
     
     <header>
   <?php 
    require('navBar.php');
    ?>

<br><br><br>

<div class="row fontSergueL" style="position:relative; top:-22px; z-index:-1;" >
<div class="col-lg-12 cWrapper" style="background-image: url('img/wrapper.png'); background-size:100% 100%;">
    <center class="fColorWhite">
      <br>
      <img src="img/logo.png" class="imgSize-md">
      <div class="h1" style="font-weight:bolder;">Welcome to space club</div>
      <h1 class="h2">O A U, Nigeria.</h1>
      <br>
     

    </center>

  </div>

</div>

 </header>


<div class="mainBody row">

  <div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-offset-1 col-sm-10 col-xs-offset-1 col-xs-10" 
  style="background-color:#303030 ; border-radius:30px 30px 0px 0px; height:60%; padding:5px; position:relative; top:-50px; z-index:1;">
   
    <div class="">

<!--row 1-->
     <div class="row resizeE" style="margin:0px 0px 0px 0px;">



  

          

          <div class="col-lg-offset- col-lg-8  fontSergueL" style="padding:15px; background:#505050 ; border-radius:25px 0px 0px 0px;">



          <!--club-->  
                 
          
            
          <div class="club">

            <!--in brief---->  
            <div class="row">
              <br><br>
              <div class=" fontSizeM bgSilver2" style="background:#909090; color:white;padding:25px; margin:0px 15px 15px 15px;">

                             
                                 <label class="webLabel-lg">How can I  be a registered member of space club O A U </label>
                                 <br><br>

                              <?php

                          $query1=mysqli_query($con,"SELECT * FROM mw");
                          while ($row1=mysqli_fetch_array($query1)) {

                         echo $row1['membership'];
                          }

                         


                              ?>
     <br>
     <hr>
     <h3 style="color:white; font-style;italic;">You can download membership form bellow.</h3>
     <br>

   <a href="drive/membershipForm.pdf" class="webBut">Download</a>
              </div>
              </div>

               <!--in brief-->  


<br><br>


  <div class="row">
   
    &nbsp;&nbsp;&nbsp;

   <a href="index" class="webBut">Home</a>
 <br><br>
  </div>
              <!--Location-->  
            </div>








              </div>





              






              

          </div>

         

          
       

     </div>
     <!--row 1-->



     <!--row 2-->

<br><br>

</div>
  </div>



</div>























<!--footer-->

<?php

$object->addSection('footer.php');


?>

    <!--footer-->






    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>




</html>


<?php


$object->closeConnection($con);

 ?>